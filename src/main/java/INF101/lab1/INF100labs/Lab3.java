package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can
 * find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void multiplesOfSevenUpTo(int n) {
        int count = 7;
        while (count <= n) {
            System.out.println(count);
            count += 7;
        }
    }

    public static void multiplicationTable(int n) {
        // kjør loopen like mange ganger som tallet
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ": ");
            for (int j = 1; j <= n; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println("");
        }
    }

    public static int crossSum(int num) {
        // gjør om til streng
        String intString = Integer.toString(num);
        // finn lengden av streng
        int length = intString.length();

        int sum = 0;
        for (int i = 0; i < length; i++) {
            char c = intString.charAt(i);
            sum = sum + Character.getNumericValue(c);
        }
        return sum;
    }

}