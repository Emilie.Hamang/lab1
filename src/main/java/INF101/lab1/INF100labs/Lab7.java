package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

public class Lab7 {

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> grid = new ArrayList<>();

        // Adding rows to the two-dimensional ArrayList
        grid.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        allRowsAndColsAreEqualSum(grid);
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        // finne sum av første rad
        int sumRowOne = 0;
        ArrayList<Integer> rowOne = grid.get(0);
        for (Integer num : rowOne) {
            sumRowOne += num;
        }

        // finne sum første kol
        int sumFirstCol = 0;
        for (ArrayList<Integer> row : grid) {
            sumFirstCol += row.get(0);
        }

        // sjekke om de er like eller ikke
        if (sumRowOne != sumFirstCol) {
            return false;
        }

        // så finner sum av hver rad og kolonne
        int rowNum = grid.size(); // finner antall rad
        int colNum = grid.get(0).size(); // finner antall kolonner

        // sjekke om rader er like
        for (int i = 1; i < rowNum; i++) {
            int sumRow = 0;
            for (int j = 0; j < colNum; j++) {
                sumRow += grid.get(i).get(j);
            }
            if (sumRow != sumRowOne) {
                return false;
            }
        }
        // sjekke om kolonner er like
        for (int i = 1; i < colNum; i++) {
            int sumCol = 0;
            for (int j = 0; j < rowNum; j++) {
                sumCol += grid.get(j).get(i);
            }
            if (sumCol != sumFirstCol) {
                return false;
            }
        }
        return true;
    }
}