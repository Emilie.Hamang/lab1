package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can
 * find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        Lab5 a = new Lab5();
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 2, 3, 4));
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(1, 5, 0, 3, 4));
        multipliedWithTwo(list);
        removeThrees(list);
        uniqueValues(list);
        a.addList(list, list2);
        addList(list, list2);
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        // for nummer i liste. Gang dette med 2 og legg til i ny liste
        ArrayList<Integer> intList = new ArrayList<>();
        for (int i : list) {
            intList.add(i * 2);
        }
        return intList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        // Gå igjennom liste. Hvis tall = 3, fjern tall
        ArrayList<Integer> noThree = new ArrayList<>();
        for (int i : list) {
            if (i != 3) {
                noThree.add(i);
            }
        }
        return noThree;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        // tar inn liste. Behold alle unike verdier og fjern duplikat
        ArrayList<Integer> uniqueList = new ArrayList<>();
        for (int i : list) {
            if (uniqueList.contains(i)) {
                continue;
            } else {
                uniqueList.add(i);
            }
        }
        return uniqueList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        // tar inn to lister
        // endre listen a ved å plusse sammen med alle verdiene i b på samme indeks
        // finn lengde a og deretter pluss sammen av og b
        int size = a.size();
        for (int i = 0; i < size; i++) {
            int aNum = a.get(i);
            int bNum = b.get(i);
            a.set(i, (aNum + bNum));
        }
    }
}