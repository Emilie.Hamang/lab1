package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can
 * find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {

    public static void main(String[] args) {
        findLongestWords("Four", "Five", "Nine");
        isEvenPositiveInt(1900);
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        // finner lengden av ordene
        int len1 = word1.length();
        int len2 = word2.length();
        int len3 = word3.length();
        // finner lengste
        int longest = Math.max(len1, Math.max(len2, len3));
        // printer ut de lengste ordene
        if (len1 == longest) {
            System.out.println(word1);
        }
        if (len2 == longest) {
            System.out.println(word2);
        }
        if (len3 == longest) {
            System.out.println(word3);
        }

    }

    public static boolean isLeapYear(int year) {
        // hvis år er delelig med 4 og ikke 100 (true)
        // hvis år er delelig med 4 og 100 og 4000 (true)
        // ellers returner ikke sant
        if ((year % 4 == 0) && (year % 100 != 0)) {
            return true;
        } else if ((year % 4 == 0) && (year % 400 == 0) && (year % 100 == 0)) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isEvenPositiveInt(int num) {
        // returnerer true hvis num er positivt partall
        // returnerer false ellers
        if ((num % 2 == 0) && (num > 0)) {
            return true;
        } else {
            return false;
        }
    }

}
